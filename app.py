import tornado.ioloop
import tornado.web
import tornado.template
from tornado import websocket
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker
from tornado.websocket import WebSocketClosedError
import os
import eyed3
from time import strftime, gmtime

from models import Song
from songform import SongForm, SumForm

from local_config import MYSQLCONNECTSTR
engine = create_engine(MYSQLCONNECTSTR, echo=True)


__UPLOADS__ = "static/uploads/"


class Fileform(tornado.web.RequestHandler):
    def get(self):
        self.render('templates/fileform.html')


class Songform(tornado.web.RequestHandler):
    def post(self):
        form = SongForm()
        fileinfo = self.request.files['filearg'][0]
        fname = fileinfo['filename']
        extn = os.path.splitext(fname)[1]
        fh = open(__UPLOADS__ + fname, 'wb')
        fh.write(fileinfo['body'])
        my = eyed3.load(__UPLOADS__ + fname)
        form.song.data = fname
        if my.tag.artist is not None:
            form.artist.data = my.tag.artist
        if my.tag.album is not None:
            form.album.data = my.tag.album
        if my.info.time_secs > 0:
            form.duration.data = strftime('%H:%M:%S', gmtime(my.info.time_secs))
        loader = tornado.template.Loader('templates')
        self.write(loader.load('songform.html').generate(complite=True, form=form))



class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        form = SumForm()
        self.write(str(form))

    def post(self):
        form = SumForm(self)
        if form.validate():
            self.write(str(form.data['a'] + form.data['b']))
        else:
            self.set_status(400)
            self.write("" % form.errors)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        q = self.get_argument('q', None)
        session = sessionmaker(bind=engine)()
        if q:
            songs = session.query(Song).filter(Song.song.contains(q) | Song.artist.contains(q) | Song.album.contains(q)).all()
        else:
            songs = session.query(Song).all()
        self.render('templates/index_page.html', songs=songs)

    def post(self):
        PlayUpdate.send_message(self.get_argument('song'))


class ArtistHandler(tornado.web.RequestHandler):
    def get(self):
        session = sessionmaker(bind=engine)()
        artists = session.query(Song.artist, func.count(Song.id)).group_by(Song.artist).all()
        self.render('templates/artists.html', artists=artists)


class AlbumHandler(tornado.web.RequestHandler):
    def get(self):
        session = sessionmaker(bind=engine)()
        albums = session.query(Song.album, func.count(Song.id)).group_by(Song.album).all()
        self.render('templates/albums.html', albums=albums)


class PlayUpdate(websocket.WebSocketHandler):
    clients = []

    def open(self, *args: str, **kwargs: str):
        PlayUpdate.clients.append(self)
        super(PlayUpdate, self).open(*args, **kwargs)
        PlayUpdate.send_message('new connect!')

    def close(self, code=None, reason=None):
        PlayUpdate.clients.remove(self)

    @classmethod
    def send_message(cls, message):
        for client in cls.clients:
            # тупо try не хватало :(
            try:
                client.write_message(message)
            except WebSocketClosedError:
                pass


def make_app():
    return tornado.web.Application([
        (r'/ws', PlayUpdate),
        (r"/", MainHandler),
        (r"/addfile", Fileform),
        (r"/ma", IndexHandler),
        (r"/addsong", Songform),
        (r'/artists', ArtistHandler),
        (r'/albums', AlbumHandler),
        (r'/(favicon.ico)', tornado.web.StaticFileHandler, {'path': "./static/"}),
        (r'/(.*)', tornado.web.StaticFileHandler, {'path': "./static/music/"}),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
