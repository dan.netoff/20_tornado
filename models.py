from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime



Base = declarative_base()


class Song(Base):
    __tablename__ = 'song'
    id = Column(Integer, primary_key=True)
    song = Column(String(140), unique=True)
    artist = Column(String(140))
    album = Column(String(140))
    duration = Column(String(140))
    uploader = Column(String(140))
    upload_date = Column(DateTime, default=datetime.now())

    def __repr__(self):
        return f'<Song({self.id}, {self.song}>'


class Message(Base):
    __tablename__ = "messages"
    id = Column(Integer, primary_key=True)
    user = Column(String(140))
    message = Column(String(140))
    message_date = Column(DateTime, default=datetime.now())


# from sqlalchemy import create_engine
# from local_config import MYSQLCONNECTSTR
# engine = create_engine('MYSQLCONNECTSTR', echo=True)
# Base.metadata.create_all(engine)
