import wtforms
from wtforms_tornado import Form
from wtforms.validators import InputRequired


class SumForm(Form):
    a = wtforms.IntegerField(validators=[InputRequired()])
    b = wtforms.IntegerField(validators=[InputRequired()])


class SongForm(Form):
    song = wtforms.StringField()
    artist = wtforms.StringField('artist', validators=[wtforms.validators.DataRequired()], default=u'Unknown')
    album = wtforms.StringField('album', validators=[wtforms.validators.DataRequired()], default=u'Untitled')
    duration = wtforms.StringField('duration', validators=[wtforms.validators.DataRequired()], default=u'1:11')
    uploader = wtforms.StringField('uploader', validators=[wtforms.validators.DataRequired()], default=u'Anonymous')
